# -*- Makefile -*-

# By default make the solvers.
default:
	$(MAKE) -C src/solvers $(MAKEFLAGS) $(MAKECMDGOALS)

again: 

clean: 
	$(RM) *~ *.pyc */*.pyc

distclean: clean
	rm -rf Cain
	rm -f Cain.app/Contents/Resources/Cain.py
	rm -rf Cain.app/Contents/Resources/gui
	rm -rf Cain.app/Contents/Resources/fio
	rm -rf Cain.app/Contents/Resources/simulation
	rm -rf Cain.app/Contents/Resources/solvers
	rm -rf Cain.app/Contents/Resources/state
	rm -f solvers/*
	rm -f src/solvers/*.d

test:
	nosetests --with-doctest -v simulation state

# Platform-independent.
pi:
	rm -rf Cain
	mkdir Cain
	mkdir Cain/gui
	mkdir Cain/fio
	mkdir Cain/simulation
	mkdir Cain/state
	cp Cain.py resourcePath.py Makefile Cain
	cp -R gui/*.py gui/*.html gui/*.png gui/icons gui/figures Cain/gui
	cp fio/*.py Cain/fio
	cp simulation/*.py Cain/simulation
	cp state/*.py Cain/state
	cp -R examples help src solvers Cain

# Put the python framework in the Mac OS X bundle.
framework:
	rm -rf Cain.app/Contents/Frameworks/Python.framework
	cp -R /Library/Frameworks/Python.framework Cain.app/Contents/Frameworks
	rm -rf Cain.app/Contents/Frameworks/Python.framework/Versions/2.6

# Mac OS X.
# First compile the executables with SCons and then rm *.o.
bundle:
	rm -rf Cain.app/Contents/Resources/Cain.py \
               Cain.app/Contents/Resources/gui \
               Cain.app/Contents/Resources/fio \
               Cain.app/Contents/Resources/simulation \
               Cain.app/Contents/Resources/src \
               Cain.app/Contents/Resources/solvers \
               Cain.app/Contents/Resources/state 
	mkdir Cain.app/Contents/Resources/gui
	mkdir Cain.app/Contents/Resources/fio
	mkdir Cain.app/Contents/Resources/simulation
	mkdir Cain.app/Contents/Resources/state
	cp Cain.py gui/icons/Cain.icns Cain.app/Contents/Resources
	cp -R gui/*.py gui/*.html gui/*.png gui/icons gui/figures Cain.app/Contents/Resources/gui
	cp fio/*.py Cain.app/Contents/Resources/fio
	cp simulation/*.py Cain.app/Contents/Resources/simulation
	cp -R examples help src solvers Cain.app/Contents/Resources
	cp state/*.py Cain.app/Contents/Resources/state

# MS Windows.
# First compile the executables and rm *.obj
win:
#	cp /c/Program\ Files/Microsoft\ Visual\ Studio\ 9.0/VC/redist/x86/Microsoft.VC90.CRT/msvcp90.dll .
	rm -rf build dist
	python setupWin.py py2exe
	cp -R gui/help.html gui/splash.png gui/icons gui/figures dist/gui
	cp -R examples help src solvers dist
	cp msvcp90.dll dist

# Old info for Python 2.5:
# Copy C:\Python25\Lib\site-src\wx\msvcp71.dll to dist
# or
# copy C:\Windows/System32/msvcp71.dll to dist
# after running "make win".
