<html>
<head>
<title>SAfSB Chapter 5</title>
</head>
<body>
<h1>SAfSB Chapter 5</h1>

<h2>Example 5.8 (Standard Modification)</h2>

<p>
The standard modification model is comprised of the two reactions 
U &rarr; W and W &rarr; U. Both reactions have mass action rate laws with
propensity factors of 2.
Open the
file <tt>examples/cain/SAfSB/Chapter5/Example-5_08-StdMod.xml</tt>.
In the methods list select TSAR, which has been defined to 
record times series with all reaction events. Click the launch button
<img src="launch.png">&nbsp; to generate a single trajectory.
Next select the TSU method, which has been defined to record 
uniformly-spaced times series data. Since we are only interested in 
plotting U, deselect the W species in the recorder panel.
In the launcher panel, set 
the number of trajectories to 1000 and then click the launch button.
Finally, select the HTB method, which records histograms at 
t = 5. Again deselect W in the recorder and then click the launch
button to generate 1000 trajectories.
</p>

<p>
Click the plot button <img src="plot.png">&nbsp; to open the plot 
configuration window. Deselect the Show column for the W species.
Then deselect the legend check box. Click the plot button to show
the single trajectory with all reaction events. The plot configuration 
window and the trajectory plot are shown below.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_07-StdMod-PCTSAR.png">
</p>

<!--To generate the plots, change the line width to 2. Save as PDF. In
Preview, save as PNG, and then resize to have width of 4 inches.-->
<p align="center">
<img src="SAfSB/Chapter5/Example-5_07-StdMod-TSAR.png">
</p>

<p>
Next we will plot the mean and standard deviation for the ensemble 
of 1000 trajectories that record times series data.
In the plot configuration window, select the &quot;Std Mod, TSU&quot;
output. in the second row of radio buttons, select Mean.
Change the line color to black by right clicking on the 
&quot;Line Color&quot; column header. Then label the axes.
Finally, click the &quot;Plot&quot; button to add the plot of
the mean with standard deviation bars to the current plot of
the single trajectory. The configuration 
window and the plot are shown below.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_07-StdMod-PCTSU.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_07-StdMod-TSU.png">
</p>


<p>
Next we will plot a histogram of the copy number of U at t = 5.
Click the Histograms tab in the plot configuration window. Since there 
is only one species and one frame, namely t = 5, we don't have to
worry about choosing between a multi-species or a multi-frame plot.
Click the &quot;Filled&quot; checkbox in the configuration table.
Uncheck the legend option and add axes labels. Finally click either
of the plotting buttons. The configuration 
window and the plot are shown below.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_07-StdMod-PCHTB.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_07-StdMod-HTB.png">
</p>


<p>
Finally, we will examine the effect of changing the stochastic rate
parameters. Clone the model &quot;Std Mod&quot; (by clicking the 
clone button <img src="editcopy.png">&nbsp; in the models list)
and name the result &quot;Std Mod 1-3&quot;. In the reactions
editor change the stochastic rate constants for the modification 
and de-modification reactions to 1 and 3, respectively.
</p>

<p>
Clone the TSU method and name the result TSU_101. In the method
editor, change the number of 
frames (the number of times at which the state is recorded) to 101.
Then clone TSU_101 and name the result TSD_101. In the method editor,
change the output category to &quot;Time Series, Deterministic&quot;.
Using the modified model, generate 5 trajectories with the
TSU_101 method and one trajectory with the TSD_101 method.
Below we plot the stochastic trajectories and the deterministic 
solution together. Note the we have specified the Y axis limits in
the plot configuration window.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_07-StdMod-1-3.png">
</p>





<h2>Example 5.8 (Heterodimerization)</h2>

<p>
Open the
file <tt>examples/cain/SAfSB/Chapter5/Example-5_08-HetDim.xml</tt>.
Generate one trajectory for the deterministic method (Det) and 
the stochastic method that records all reactions (All).
Generate 1000 trajectories for the time series method (Stoch)
and the histogram method (Hist). Note that the copy number of X1
is always the same as X2, so you may deselect X2 in the recorder panel
before launching the simulations. Below we plot the deterministic
solution in a dashed line, the single stochastic trajectory in colored
lines, and the mean and standard deviations of the ensemble of 1000
stochastic trajectories in solid black lines.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_08-HetDim-TS.png">
</p>

<p>
Next we plot the histograms at t = 1.5.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_08-HetDim-H.png">
</p>





<h2>Example 5.9 (Lotka-Volterra)</h2>

<p>
Open the file <tt>examples/cain/SAfSB/Chapter5/Example-5_09-LotVol.xml</tt>.
Generate a single trajectory. Below we plot the results. Note that 
we have used the &quot;Legend Label&quot; column in the plot
configuration window to specify the names in the legend.
</p>


<p align="center">
<img src="SAfSB/Chapter5/Example-5_09-LotVol-TS.png">
</p>

<p>
You may clone the model to try out different initial conditions. Note
that you may not modify a model that has dependent simulation
output. By cloning, you can compare the different outputs.
</p>





<h2>Example 5.10 (Enzyme Kinetic Reaction)</h2>

<p>
Open the
file <tt>examples/cain/SAfSB/Chapter5/Example-5_10-EnzKin.xml</tt>.
The two models differ in the initial amounts of the species. The Large
model has initial amounts of 500 and 200 for the species S and E,
respectively. For the Small model, the corresponding initial amounts 
are 50 and 20. For the Large model, we will use the time interval
0..50, while for the Small model we will use 0..100.
Generate five trajectories for the following model/method pairs:
Large/Fine50 and Small/Fine100.
Below we plot the results. We note that the small
system is noticeably noisier.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_10-EnzKin-TL.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_10-EnzKin-TS.png">
</p>

<p>
Generate 10,000 trajectories for the Large and Small models using
the Coarse50 and Coarse100 methods, respectively. Below we plot the
means and standard deviations.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_10-EnzKin-ML.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_10-EnzKin-MS.png">
</p>


<p>
To collect histogram data at the end time, generate 10,000
trajectories for the Large and Small models using
the Hist50 and Hist100 methods, respectively. Below we show the plot
configuration windows and plot the histograms. We generate the
plots by clicking the &quot;Plot Together&quot; button.
Note that for the Small model the histograms overlap so 
we make them translucent by setting the Alpha value to 0.5.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_10-EnzKin-PCL.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_10-EnzKin-HL.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_10-EnzKin-PCS.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_10-EnzKin-HS.png">
</p>








<h2>Example 2.12 (Schlogl)</h2>

<p>
We use the Schlogl model in the 
<a href="VisualizationPlottingHistograms.htm">Plotting Histograms</a>
section of the 
<a href="Visualization.htm">Visualization and Analysis</a>
chapter. The stochastic rate constants differ, but the behavior is 
essentially the same.
</p>






<h2>Example 2.13 (Gene Regulation)</h2>

<p>
Open the
file <tt>examples/cain/SAfSB/Chapter5/Example-5_13-GeneReg.xml</tt>.
Generate a single trajectory for each model using the All method.
Then generate 1000 trajectories each using the Uniform method.
Below we plot a single trajectory and the mean abundance for the
default model, the model with decreased k<sub>m</sub>, and the
model with decreased k<sub>m</sub> and increased k<sub>p</sub>.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_13-GeneReg-1.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_13-GeneReg-2.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_13-GeneReg-3.png">
</p>

<p>
To measure the noise in each model, generate a single trajectory for
each using the Steady method. Here we record the state for a time
interval of 10,000 seconds. For each simulation output, click
the table button <img src="x-office-spreadsheet.png">&nbsp;
in the simulation output panel, then select the
mean and standard deviation option. The resulting tables are
shown below. Dividing the standard deviations by the means, we see
that the second and third models are noisier than the first.
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_13-GeneReg-S1.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_13-GeneReg-S2.png">
</p>

<p align="center">
<img src="SAfSB/Chapter5/Example-5_13-GeneReg-S3.png">
</p>


</body>
</html>
